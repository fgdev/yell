-- --------------------------------------------------------
-- Host:                         192.168.106.15
-- Server version:               10.2.2-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dev_biztorg.y_author
CREATE TABLE IF NOT EXISTS `y_author` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dev_biztorg.y_author: ~0 rows (approximately)
DELETE FROM `y_author`;
/*!40000 ALTER TABLE `y_author` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_author` ENABLE KEYS */;

-- Dumping structure for table dev_biztorg.y_book
CREATE TABLE IF NOT EXISTS `y_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dev_biztorg.y_book: ~0 rows (approximately)
DELETE FROM `y_book`;
/*!40000 ALTER TABLE `y_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_book` ENABLE KEYS */;

-- Dumping structure for table dev_biztorg.y_mm_author_book
CREATE TABLE IF NOT EXISTS `y_mm_author_book` (
  `author_id` int(10) unsigned NOT NULL,
  `book_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`author_id`,`book_id`),
  KEY `fk_mm_author_book_indx2` (`book_id`),
  CONSTRAINT `fk_mm_author_book_indx1` FOREIGN KEY (`author_id`) REFERENCES `y_author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_mm_author_book_indx2` FOREIGN KEY (`book_id`) REFERENCES `y_book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dev_biztorg.y_mm_author_book: ~0 rows (approximately)
DELETE FROM `y_mm_author_book`;
/*!40000 ALTER TABLE `y_mm_author_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `y_mm_author_book` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
