<?php
	/**
	 * Created by PhpStorm.
	 * User: s.panyukov
	 */

	namespace apilocal\modules\redactor\interfaces;


	interface FigureInterface
	{

		public function process();

	}