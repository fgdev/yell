<?php
	/**
	 * Created by PhpStorm.
	 * User: s.panyukov
	 */

	namespace apilocal\modules\redactor\models\shapes;

	use apilocal\modules\redactor\interfaces\FigureInterface;
	use apilocal\modules\redactor\models\BaseFigure;

	class FigureSquare extends BaseFigure implements FigureInterface
	{


		private function formatResponse($data, $format)
		{
			#some logic here
			return ['data' => $data, 'format' => $format, 'title' => 'This is a square'];
		}

		/**
		 * @return array|bool
		 */
		public function process()
		{
			if($this->params['color'])
			{
				#some logic here
				$data = $this->params['color'];
				return $this->formatResponse($data, $this->responseFormat);
			}
			else
				return false;
		}


	}