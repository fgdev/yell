<?php
	/**
	 * Created by PhpStorm.
	 * User: s.panyukov
	 * Date: 13.12.2016
	 * Time: 11:42
	 */

	namespace apilocal\modules\redactor\models;

	class Redactor
	{

		/**
		 *  run action
		 * @param $class
		 * @param $params
		 * @return bool
		 */
		public static function run($class, $params)
		{
			if (!class_exists($class) || !is_array($params)) return false;

			$model = new $class($params);

			return $model->process();
		}

	}
