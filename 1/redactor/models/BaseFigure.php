<?php
	/**
	 * Created by PhpStorm.
	 * User: s.panyukov
	 */

	namespace apilocal\modules\redactor\models;




	class BaseFigure
	{
		/**
		 * response formats
		 */
		const RESPONSE_FORMAT_IMAGE = 'IMAGE';
		const RESPONSE_FORMAT_ARRAY = 'ARRAY';

		/**
		 * incoming params
		 * @var array
		 */
		public $params = [];

		/**
		 * @var
		 */
		public $responseFormat = self::RESPONSE_FORMAT_IMAGE;

		/**
		 * @return array
		 */
		public static function getResponseFormats()
		{
			return [
					self::RESPONSE_FORMAT_IMAGE => self::decodeResponseFormat(self::RESPONSE_FORMAT_IMAGE),
					self::RESPONSE_FORMAT_ARRAY => self::decodeResponseFormat(self::RESPONSE_FORMAT_ARRAY),
			];
		}

		/**
		 * @param $format
		 * @return string
		 */
		public static function decodeResponseFormat($format)
		{
			$format = strtoupper($format);
			switch ($format)
			{
				case self::RESPONSE_FORMAT_IMAGE:
					return 'изображение';
					break;
				case self::RESPONSE_FORMAT_ARRAY:
					return 'массив';
					break;
				default:
					return '-';
					break;
			}
		}

		/**
		 * BaseFigure constructor.
		 * @param $params
		 * @param $responseFormat
		 */
		public function __construct($params)
		{
			$this->params = $params;
			$this->responseFormat = ($params['responseFormat'] && array_key_exists($params['responseFormat'], self::getResponseFormats()))? $params['responseFormat'] : self::RESPONSE_FORMAT_ARRAY;
		}


	}