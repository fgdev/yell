<?php
/**
 * Created by JetBrains PhpStorm.
 * User: s.panyukov
 */

	namespace apilocal\modules\redactor\controllers;

	use apilocal\modules\redactor\models\Redactor;
	use yii\base\Controller;

	class TestController extends Controller
	{

		public function actionIndex()
		{
			$shapes = [
					[
							'type' => 'circle',
							'params' => [
									'radius' => 100,
									'color' => '#001122',
									'line' => 1
							]
					],
					[
							'type' => 'square',
							'params' => [
									'color' => '#003355',
									'line' => 2,
									'length' => 100,
									'responseFormat' => 'IMAGE'
							]
					],
			];
			$aResult = [];

			foreach($shapes as $k => $vShape)
			{
				if(!$vShape['type'] || !$vShape['params'])
					continue;
				$aResult[] =  Redactor::run('apilocal\modules\redactor\models\shapes\Figure'.ucfirst($vShape['type']), $vShape['params']);
			}

			#return var_dump($aResult);
			return $aResult;

		}

	}

