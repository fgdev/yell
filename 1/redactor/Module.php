<?php
	namespace apilocal\modules\redactor;

	use Yii;
	use yii\base\Module as BaseModule;

	/**
	 * Redactor Module
	 */
	class Module extends BaseModule
	{

		/**
		 * init
		 */
		public function init()
		{
			parent::init();

		}

	}