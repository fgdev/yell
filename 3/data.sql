-- --------------------------------------------------------
-- Host:                         192.168.106.15
-- Server version:               10.2.2-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dev_biztorg.y_data
CREATE TABLE IF NOT EXISTS `y_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(6) NOT NULL,
  `date` date NOT NULL,
  `value` int(10) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table dev_biztorg.y_data: ~4 rows (approximately)
DELETE FROM `y_data`;
/*!40000 ALTER TABLE `y_data` DISABLE KEYS */;
INSERT INTO `y_data` (`id`, `type`, `date`, `value`) VALUES
	(1, 'photo', '2015-02-02', 1240),
	(2, 'image', '2015-02-02', 5609),
	(3, 'photo', '2015-02-01', 1190),
	(4, 'review', '2015-02-02', 3600);
/*!40000 ALTER TABLE `y_data` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
